package com.classpath.streams.util;

import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StreamsDemo {
	
	public static void main(String[] args) {
	
		Integer[] values = {23, 24, 45, 67, 76, 43, 22, 14, 56, 17, 19, 11, 13, 15};
		
		Predicate<Integer> lessThan20 = value -> value < 20;
		
		
		Predicate<Integer> moreThan20 = lessThan20.negate();
		Consumer<Integer> printValue = val -> System.out.println("Value less than 20 :: "+ val);
		
		Stream<Integer> intStream = Stream.of(values);
		
		//Stream<Integer> valuesLessThan20 = intStream.filter(lessThan20);
		
		//valuesLessThan20.forEach(printValue);
		
		/*
		 * 1. Print out all the numbers between 20 and 30 - filter
		 * 2. Print all the numbers in the days format (value * 365) - map
		 * 3. Print the max age of the Person who is between 20 and 40 - short circuit
		 * 4. Calculate the total ages of all the values. - reduce
		 */
		
		 //Print out all the numbers between 20 and 30 - filter
			/*
			 * Predicate<Integer> between20And30 = value -> value > 20 && value < 30;
			 * Stream<Integer> valuesbetween20And30 = intStream.filter(between20And30);
			 * valuesbetween20And30.forEach(printValue);
			 */
		
		//Print all the numbers in the days format (value * 365) - map
		
		/*
		 * Function<Integer, Integer> mapYearsToDays = year -> year * 365;
		 * 
		 * intStream .map(mapYearsToDays) .forEach(printValue);
		 */
		
		//Print the max age of the Person who is between 20 and 40 - short circuit
		/*
		 * Predicate<Integer> between20And40 = value -> value > 20 && value < 40;
		 * 
		 * int maxValue =
		 * intStream.filter(between20And40).max(Integer::compare).orElse(0);
		 * System.out.println("Max value :: "+ maxValue);
		 */
		
		// Calculate the total ages of all the values. - reduce
		BinaryOperator<Integer> reducingSum = (v1, v2) -> v1 + v2;
		int totalValue = intStream.reduce(reducingSum).orElse(0);
		System.out.println("Total value -> "+totalValue);
	}

}
